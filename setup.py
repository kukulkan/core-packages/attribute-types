from setuptools import setup, find_packages


setup(
    name='attribute-types',
    version='0.1.0',
    description='Core attribute types for Kukulkan model.',
    author='',
    author_email='',
    license='MIT',
    py_modules=['kukulkan_attribute_types'],
    entry_points={
        'kukulkan.attributes': [
            'Float=kukulkan_attribute_types:Float',
            'Integer=kukulkan_attribute_types:Integer',
            'String=kukulkan_attribute_types:String',
            'Boolean=kukulkan_attribute_types:Boolean',
            'Enum=kukulkan_attribute_types:Enum',
        ]
    },
    url='https://gitlab.com/kukulkan/core-packages/attribute-types',
    download_url='git+https://gitlab.com/kukulkan/core-packages/attribute-types.git#egg=attribute-types',
    install_requires=['kukulkan'],
    dependency_links=['git+https://gitlab.com/kukulkan/kukulkan#egg=kukulkan']
)
