from kukulkan.model.attribute import Attribute


class Float(Attribute):
    """A `float` `Attribute`."""

    def cast_value(self, value):
        return float(value)


class Integer(Attribute):
    """An `int` `Attribute`."""

    def cast_value(self, value):
        return int(value)


class String(Attribute):
    """A `str` `Attribute`."""

    def cast_value(self, value):
        return str(value)


class Boolean(Attribute):
    """A `bool` `Attribute`."""

    def cast_value(self, value):
        return bool(value)


class Enum(Attribute):
    """An `Attribute` with value choices.

    You should override the ``choices`` attribute in your class or
    instances to allow this attribute to be set.

    Trying to set the value to anything else that what ``choices``
    allows will result in a `ValueError`.
    """

    choices = []

    def cast_value(self, value):
        """Ensure the passed ``value`` is contained in ``choices``.

        :raise ValueError: When a forbidden value is passed.
        """
        if value not in self.choices:
            raise ValueError(
                'Value should be one of {}'.format(', '.format(self.choices))
            )
        return value
